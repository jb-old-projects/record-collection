/********************************************************************************************************/
/*						Autor:							Jakub Biskup									*/
/*						Kierunek:						Informatyka										*/
/*						Rok studi�w:					I												*/
/*						Semestr:						I												*/
/*						Data: ostatniej modyfikacji:	25 stycznia 2016, 21:35							*/
/********************************************************************************************************/

#pragma once
#include <iostream>
#include <string>
#include <conio.h>
#include <Windows.h>
#include <time.h>
#include <iomanip>

using namespace std;

struct track
{
	track(string _song);
	string song;
};

class records
{
public:

	struct album
	{
		album::album(int _amount, track **tracklist, string _artist, string _title, int _year, string _genre, int _ID);
		album::~album();
		int amount;
		track **tracklist;
		string artist;
		string title;
		int year;
		string genre;
		int ID;
		album *next;
	};

	album *top = 0;
	album *back;

	/******************************************************************************************************/

	//void push_s(string song);
	void push(int amount, track **tracklist, string artist, string title, int year, string genre, int ID);
	void show();
	void show_s();
	void search();
	void edit();
	void erase();
	int count();
	void hit(int number);
};