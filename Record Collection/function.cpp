/********************************************************************************************************/
/*						Autor:							Jakub Biskup									*/
/*						Kierunek:						Informatyka										*/
/*						Rok studi�w:					I												*/
/*						Semestr:						I												*/
/*						Data: ostatniej modyfikacji:	25 stycznia 2016, 21:35							*/
/********************************************************************************************************/

#include "function.h"

records x;

void Menu()
{ // procedura obs�uguj�ca menu
	char q;
	do {
		system("cls");

		cout << "                     Wybierz operacje: " << endl << "      ********************************************" << endl
			<< "      *     1 => Wyswietl aktualna liste plyt    *" << endl
			<< "      *     2 => Dodaj album                     *" << endl
			<< "      *     3 => Usun album                      *" << endl
			<< "      *     4 => Edytuj album                    *" << endl
			<< "      *     5 => Wyszukaj album (informacje)     *" << endl
			<< "      *     6 => Album tygodnia                  *" << endl
			<< "      *     7 => Wczytywanie bazy danych         *" << endl
			<< "      *     8 => Zapisywanie bazy danych         *" << endl
			<< "      *     9 => Zakoncz                         *" << endl
			<< "      ********************************************" << endl;

		q = _getch();

		switch (q)
		{
			case '1':
			{
				x.show();
				system("pause");
				break;
			}
			case '2' :
			{
				push_tmp();
				system("pause");
				break;
			}
			case '3':
			{
				x.erase();
				break;
			}
			case '4':
			{
				x.edit();
				break;
			}
			case '5':
			{
				x.search();
				break;
			}
			case '6':
			{
				srand(time(0));
				int y = x.count();

				if (!x.top)
				{
					system("cls");
					cout << "\t\t\t ******************" << endl;
					cout << "\t\t\t * Album tygodnia *" << endl;
					cout << "\t\t\t ******************" << endl;

					cout << endl << " Brak albumow w bazie!" << endl;
					system("pause");
					break;
				}

				int z = rand() % y + 0;
				x.hit(z+1);
				system("pause");
				break;
			}
			case '7':
			{
				FileRead();
				break;
			}
			case '8':
			{
				FileWrite();
				break;
			}
			case '9' :
			{
				exit(0);
			}
		}
			
	   } while (true);
}

void Exit(int status)
{ // procedura, kt�rej jedynym zadaniem jest zako�czenie pracy programu
	system("pause");
}

void push_tmp()
{ // procedura, kt�rej celem jest odpowiednie przygotowanie danych podanych przez u�ytkownika do dodania ich do bazy
	system("cls");
	cout << "\t\t\t ********************" << endl;
	cout << "\t\t\t * Dodawanie albumu *" << endl;
	cout << "\t\t\t ********************" << endl;

	string title_tmp;
	string artist_tmp;
	int  amount_tmp;
	int year_tmp;
	string genre_tmp;
	string song_tmp;
	title_tmp.resize(16);
	artist_tmp.resize(16);
	genre_tmp.resize(16);
	song_tmp.resize(16);
	cout << " Wszelkie spacje zastap podkreslnikami '_' !" << endl;

	cout << endl << " Podaj nazwe albumu: ";
	cin >> title_tmp;
	cout << endl << " Podaj wykonawce: ";
	cin >> artist_tmp;
	cout << endl << " Podaj gatunek: ";
	cin >> genre_tmp;

	cout << endl << " Podaj rok wydania: ";
	cin >> year_tmp;

	cout << endl << " Podaj ilosc utworow: ";
		cin >> amount_tmp;
	track **tracklist_tmp = new track*[amount_tmp];


	for (int i = 0; i < amount_tmp; i++)
	{
		cout << endl << " Podaj nazwe utworu nr " << i + 1 << ": ";
		cin >> song_tmp;
		tracklist_tmp[i] = new track(song_tmp);
	}

	x.push(amount_tmp, tracklist_tmp, artist_tmp, title_tmp, year_tmp, genre_tmp, 3213);
	cout << endl << " Album zostal dodany pomyslnie!" << endl;
}

void FileRead()
{ // procedura, kt�ra ma na celu wczytanie bazy danych z pliku binarnego 
	//(mo�e zar�wno nadpisa� obecn� baz� jak i dopisa� wczytywan� baz� do obecnej)
	system("cls");
	cout << "\t\t\t ***************************" << endl;
	cout << "\t\t\t * Wczytywanie bazy danych *" << endl;
	cout << "\t\t\t ***************************" << endl;
	int wyb;
	cout	<< "           Wybierz operacje: " << endl
			<< " ***********************************" << endl
			<< " * 1 => Nadpisz istniejaca baze    *" << endl
			<< " * 2 => Dopisz do istniejacej bazy *" << endl
			<< " ***********************************" << endl;

	wyb = _getch();

	switch (wyb)
	{

	case '1':
	{
		system("cls");
		cout << "\t\t\t ***************************" << endl;
		cout << "\t\t\t * Wczytywanie bazy danych *" << endl;
		cout << "\t\t\t ***************************" << endl;

		delete x.top;
	}
	case '2':
	{
		system("cls");
		cout << "\t\t\t ***************************" << endl;
		cout << "\t\t\t * Wczytywanie bazy danych *" << endl;
		cout << "\t\t\t ***************************" << endl;

		string input;
		cout << " Podaj nazwe pliku wejsciowego: ";
		cin >> input;
		ifstream read;
		read.open(input, ios::binary);

		if (!read.is_open())
		{
			cout << " Nie udalo sie otworzyc pliku wejsciowego! Sprobuj jeszcze raz!" << endl;
			system("pause");
			return;
		}

		records::album *y = x.top;
		string title_tmp, artist_tmp, genre_tmp, song_tmp;
		title_tmp.resize(24, 0);
		artist_tmp.resize(24, 0);
		genre_tmp.resize(24,0);
		song_tmp.resize(24, 0);
		int amount_tmp, year_tmp, ID_tmp;
		while (true)
		{
			if (!read.read((char*)&title_tmp[0], 24))
				break;

			read.read((char*)&artist_tmp[0], 24);
			read.read((char*)&amount_tmp, 4);
			track **tracklist_tmp = new track*[amount_tmp];
			read.read((char*)&genre_tmp[0], 24);
			read.read((char*)&year_tmp, 4);
			read.read((char*)&ID_tmp, 4);
			for (int i = 0; i < amount_tmp; i++)
			{
				read.read((char*)&song_tmp[0], 24);
				tracklist_tmp[i] = new track(song_tmp);
			}
			x.push(amount_tmp, tracklist_tmp, artist_tmp, title_tmp, year_tmp, genre_tmp, ID_tmp);
		}

		read.close();
		break;
	}

	default :
	{
		cout << " Wczytywanie bazy nie udalo sie!" << endl;
		system("pause");
		return;
	}

	}
	cout << " Wczytywanie bazy przebieglo pomyslnie!" << endl;
	system("pause");
	return;
}

void FileWrite()
{ // procedura, kt�rej zadaniem jest zapisywanie bazy danych do pliku binarnego o dowolnej nazwie podanej przez uzytkownika
	system("cls");
	cout << "\t\t\t ***************************" << endl;
	cout << "\t\t\t * Zapisywanie bazy danych *" << endl;
	cout << "\t\t\t ***************************" << endl;
	string output;
	cout << " Podaj nazwe pliku wyjsciowego: ";
	cin >> output;
	ofstream write;
	write.open(output, ios::binary);
	if (!write.is_open())
	{
		cout << " Nie udalo sie otworzyc pliku wyjsciowego! Sprobuj jeszcze raz!";
		system("pause");
		FileWrite();
	}
	
	records::album *y = x.top;

	for (int i = 0; i < x.count(); i++)
	{
		y->title.resize(24, 0);
		write.write(y->title.c_str(), 24);
		write.write(y->artist.c_str(), 24);
		write.write((char*)&y->amount, sizeof(int));
		write.write(y->genre.c_str(), 24);
		write.write((char*)&y->year, sizeof(int));
		write.write((char*)&y->ID, sizeof(int));

		for (int j = 0; j < y->amount; j++)
		{
			write.write(y->tracklist[j]->song.c_str(), 24);
		}
		y = y->next;
	}

	cout << " Zapisywanie bazy przebieglo pomyslnie!" << endl;
	system("pause");

}