/********************************************************************************************************/
/*						Autor:							Jakub Biskup									*/
/*						Kierunek:						Informatyka										*/
/*						Rok studi�w:					I												*/
/*						Semestr:						I												*/
/*						Data: ostatniej modyfikacji:	25 stycznia 2016, 21:35							*/
/********************************************************************************************************/

#pragma once
#include "record.h"
#include <fstream>
#include <cstdio>

void exit(int status);
void Menu();
void FileRead();
void FileWrite();
void push_tmp();