/********************************************************************************************************/
/*						Autor:							Jakub Biskup									*/
/*						Kierunek:						Informatyka										*/
/*						Rok studi�w:					I												*/
/*						Semestr:						I												*/
/*						Data: ostatniej modyfikacji:	25 stycznia 2016, 21:35							*/
/********************************************************************************************************/


#include "record.h"


records::album::album(int _amount, track **_tracklist, string _artist, string _title, int _year, string _genre, int _ID)
{ //konstruktor klasy records
	amount = _amount;
	tracklist = _tracklist;
	artist = _artist;
	title = _title;
	year = _year;
	genre = _genre;
	ID = _ID;
	next = 0;
}
records::album::~album()
{ // destruktor klasy records
	for (int i = 0; i < amount; i++)
		delete &tracklist[i]->song;
	delete next;
}

track::track(string _song)
{ // konstruktor klasy track
	song = _song;
}

void records::push(int amount, track **tracklist, string artist, string title, int year, string genre, int ID)
{ // metoda klasy records, kt�rej zadaniem jest dodawanie nowych element�w do listy album�w
	if (top)
	{
		back->next = new album(amount, tracklist, artist, title, year, genre, ID);
		back = back->next;
	}
	else
	{
		top = new album(amount, tracklist, artist, title, year, genre, ID);
		back = top;
	}
}

void records::show()
{ // metoda klasy records, kt�rej zadaniem jest pokazanie obecnego stanu listy album�w
	system("cls");
	cout << "\t\t\t ******************" << endl;
	cout << "\t\t\t * Aktualna lista *" << endl;
	cout << "\t\t\t ******************" << endl;

	album *y = top;
	if (y == 0)
	{
		cout << "\t\t   Nie ma zadnego albumu w bazie" << endl;
		return;
	}
	cout << " Nazwa albumu | Wykonawca | Utworow | Rok wydania | Gatunek " << endl;
	while (y)
	{
		cout << setw(10) << y->title.c_str() << setw(12) << y->artist.c_str() << setw(10) << y->amount << setw(11) << y->year << setw(13) << y->genre.c_str() << endl;
		if (y->next == 0)
			break;
		else
		y = y->next;
	}
}

void records::search()
{ // metoda klasy records, kt�rej zadaniem jest znalezienie podanego przez u�ytownika albumu i podanie informacji o nim
	system("cls");
	cout << "\t\t\t ***********************" << endl;
	cout << "\t\t\t * Wyszukaj informacje *" << endl;
	cout << "\t\t\t ***********************" << endl;
	cout << " Podaj nazwe albumu, ktorego chcesz wyszukac: ";
	string title;
	cin >> title;

	album *y = top;
	track *z = 0;
	if (top)
	{
		while (true)
		{
			if (y->title.c_str() == title)
			{
				cout << " Nazwa albumu | Wykonawca | Utworow | Rok wydania | Gatunek " << endl;
				cout << setw(10) << y->title.c_str() << setw(12) << y->artist.c_str() << setw(10) << y->amount << setw(11) << y->year << setw(13) << y->genre.c_str() << endl;
				cout << endl << " Lista utworow: " << endl;

				for (int i = 0; i < y->amount; i++)
				{
					cout << i + 1 << ". " << y->tracklist[i]->song.c_str() << endl;
				}

				system("pause");
				return;
			}
			else if (y->next == 0)
				break;
			else
				y = y->next;
		}
	}
	cout << endl << " Podany album nie istnieje!" << endl;
	system("pause");
}

void records::erase()
{ // metoda klasy records, kt�rej zadaniem jest usuni�cie wybranego przez u�ytkownika albumu
	system("cls");
	cout << "\t\t\t ******************" << endl;
	cout << "\t\t\t * Usuwanie wpisu *" << endl;
	cout << "\t\t\t ******************" << endl;
	cout << " Wpisz nazwe albumu, ktory chcesz usunac: ";
	string title;
	cin >> title;

		if (top)
		{
			album *y = top;
			album *tmp;

			if (y->title.c_str() == title)
			{
				tmp = y->next;

				for (int i = 0; i<y->amount; i++)
					delete y->tracklist[i];

				delete y;
				top = tmp;
				cout << endl << "Album zostal pomyslnie usuniety!" << endl;
				system("pause");
				return;
			}

			while (true)
			{
				if (y->next->title.c_str() == title)
				{
					tmp = y->next->next;

					for (int i = 0; i<y->next->amount; i++)
						delete y->next->tracklist[i];

					delete y->next;
					y->next = tmp;
					cout << endl << "Album zostal pomyslnie usuniety!" << endl;
					system("pause");
					return;
				}
				else
					y = y->next;
			}
		}
		cout << "Nie ma takiego albumu w bazie!" << endl;
		system("pause");
}

int records::count()
{ // metoda klasy records, kt�rej zadaniem jest zliczenie ilo�ci album�w znajduj�cych si� w bazie
	int x = 0;
	album *y = top;
	if (top)
	{
		while (y)
		{
			x++;
			y = y->next;
		}
		return x;
	}
	else
		return x;
}

void records::hit(int number)
{ // metoda klasy records, kt�rej zadaniem jest wylosowanie albumu oraz podanie informacji o nim
	system("cls");
	cout << "\t\t\t ******************" << endl;
	cout << "\t\t\t * Album tygodnia *" << endl;
	cout << "\t\t\t ******************" << endl;

	album *y = top;
	if(top)
	{
		for (int i = 0; i < number; i++)
		{
			if (i + 1 == number)
			{
				cout << " Nazwa albumu | Wykonawca | Utworow | Rok wydania | Gatunek " << endl;
				cout << setw(10) << y->title.c_str() << setw(12) << y->artist.c_str() << setw(10) << y->amount << setw(11) << y->year << setw(13) << y->genre.c_str() << endl;
				cout << endl << " Lista utworow: " << endl;

				for (int i = 0; i < y->amount; i++)
				{
					cout << i + 1 << ". " << y->tracklist[i]->song.c_str() << endl;
				}

			}
			else
				y = y->next;
		}
	}
}

void records::edit()
{ // metoda klasy records, kt�rej zadaniem jest edycja dowolnie wybranego elementu albumu (w tym tak�e nazwy piosenki, znajduj�cej si� na li�cie utwor�w)
	system("cls");
	cout << "\t\t\t ****************" << endl;
	cout << "\t\t\t * Edycja wpisu *" << endl;
	cout << "\t\t\t ****************" << endl;

	int number;
	cout << "     Wybierz co chcesz edytowac: " << endl << " **********************************" << endl
		<< " *      1 => Nazwe albumu         *" << endl
		<< " *      2 => Tworce               *" << endl
		<< " *      3 => Gatunek              *" << endl
		<< " *      4 => Nazwe utworu         *" << endl
		<< " *      5 => Rok wydania          *" << endl
		<< " **********************************" << endl;

	number = _getch();


	switch (number)
	{
	case '1':
	{
		system("cls");
		cout << "\t\t\t ****************" << endl;
		cout << "\t\t\t * Edycja wpisu *" << endl;
		cout << "\t\t\t ****************" << endl;
		string name, name2;
		album *y = top;
		cout << " Podaj tytul albumu: ";
		cin >> name;
		cout << endl;
		cout << " Podaj tytul, na jaka chcesz zmienic: ";
		cin >> name2;

		while (y)
		{
			if (y->title.c_str() == name)
			{
				y->title = name2;
				cout << endl << " Tytul zmieniono pomyslnie!" << endl;
				system("pause");
				return;
			}
			else
				y = y->next;
		}
		cout << endl << " Nie ma takiego albumu!" << endl;
		system("pause");
		break;
	}
	case '2':
	{
		system("cls");
		cout << "\t\t\t ****************" << endl;
		cout << "\t\t\t * Edycja wpisu *" << endl;
		cout << "\t\t\t ****************" << endl;
		string art2, alb;
		album *y = top;
		cout << " Podaj nazwe albumu: ";
		cin >> alb;
		cout << endl;
		cout << " Podaj wykonawce, na ktorego chcesz zmienic: ";
		cin >> art2;

		while (y)
		{

			if (y->title.c_str() == alb)
			{
				y->artist = art2;
				cout << endl << " Wykonawce zmieniono pomyslnie!" << endl;
				system("pause");
				return;
			}
			else
				y = y->next;
		}
		cout << endl << " Nie ma takiego albumu!" << endl;
		system("pause");
		break;
	}
	case '3':
	{
		system("cls");
		cout << "\t\t\t ****************" << endl;
		cout << "\t\t\t * Edycja wpisu *" << endl;
		cout << "\t\t\t ****************" << endl;
		string alb, gen2;
		album *y = top;
		cout << " Podaj tytul albumu: ";
		cin >> alb;
		cout << endl;
		cout << " Podaj gatunek, na jaki chcesz zmienic: ";
		cin >> gen2;

		while (y)
		{
			if (y->title.c_str() == alb)
			{
				y->genre = gen2;
				cout << endl << " Gatunek zmieniono pomyslnie!" << endl;
				system("pause");
				return;
			}
			else
				y = y->next;
		}
		cout << endl << " Nie ma takiego albumu!" << endl;
		system("pause");
		break;
	}
	case '4':
	{
		system("cls");
		cout << "\t\t\t ****************" << endl;
		cout << "\t\t\t * Edycja wpisu *" << endl;
		cout << "\t\t\t ****************" << endl;
		string alb, name, name2;
		album *y = top;
		cout << " Podaj tytul albumu, na ktorym znajduje sie piosenka: ";
		cin >> alb;
		cout << endl;
		cout << " Podaj tytul piosenki, ktora chcesz zmienic: ";
		cin >> name;
		cout << endl;
		cout << " Podaj tytul piosenki, na jaki chcesz zmienic: ";
		cin >> name2;

		while (y)
		{
			if (y->title.c_str() == alb)
			{
				for (int i = 0;true; i++)
				{
					if (y->tracklist[i]->song == name)
					{
						y->tracklist[i]->song = name2;
						cout << endl << " Tytul piosenki zmieniono pomyslnie!" << endl;
						system("pause");
						return;
					}
				}
			}
			else
				y = y->next;
		}
		cout << endl << " Nie ma takiego albumu!" << endl;
		system("pause");
		break;
	}
	case '5':
	{
		system("cls");
		cout << "\t\t\t ****************" << endl;
		cout << "\t\t\t * Edycja wpisu *" << endl;
		cout << "\t\t\t ****************" << endl;
		string alb;
		int year2;
		album *y = top;
		cout << " Podaj tytul albumu: ";
		cin >> alb;
		cout << endl;
		cout << " Podaj rok wydania, na jaki chcesz zmienic: ";
		cin >> year2;

		while (y)
		{
			if (y->title.c_str() == alb)
			{
				y->year = year2;
				cout << endl << " Rok wydania zmieniono pomyslnie!" << endl;
				system("pause");
				return;
			}
			else
				y = y->next;
		}
		cout << endl << " Nie ma takiego albumu!" << endl;
		system("pause");
		break;
	}
	}

}